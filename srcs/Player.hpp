#ifndef PLAYER_HPP__
#define PLAYER_HPP__

#include <SFML/Graphics.hpp>
#include <memory>

#include "TextureManager.hpp"

namespace player {
    class Player {
        public:
            Player(char key, int posx, int posy, TextureManager::TexturePtr &self, TextureManager::TexturePtr &broom);
            Player(char key, sf::Vector2f pos, TextureManager::TexturePtr &self, TextureManager::TexturePtr &broom);
            ~Player() {}

            void reset();

            void draw(sf::RenderWindow &);
            Player & move();
            Player & stop();
            Player & rotate(float angle);
            Player & updatePos();

            sf::Vector2f getBroomPos();
            sf::Vector2f getNextPos();
            sf::Vector2f getPos();
            sf::Vector2f getSpeed();
            float getAngle();

        private:
            float _angle;
            sf::Vector2f _initial_pos;
            sf::Vector2f _pos;
            sf::Vector2f _speed;
            sf::Sprite _self;
            std::shared_ptr<sf::Texture> _self_text;
            sf::Sprite _broom;
            std::shared_ptr<sf::Texture> _broom_text;
    };

    using PlayerPtr = std::unique_ptr<Player>;
}

#endif
