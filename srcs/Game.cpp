#include <iostream>
#include "Game.hpp"

Game::Game(sf::VideoMode mode) :
    _win(mode, "Exit"), _level(1), _textMngr(), _mapLoader(_textMngr)
{
    _win.setFramerateLimit(30);
    _musicPlayer.openFromFile("resources/music/Jam.wav");
    _musicPlayer.setLoop(true);
    _musicPlayer.setLoopPoints(sf::Music::TimeSpan(sf::milliseconds(41739), _musicPlayer.getDuration()));
    _musicPlayer.play();
}

void Game::run() {
    int consec_press = 0;
    bool pressed = false;
    load_level();

    while (_win.isOpen()) {
        sf::Event evt;

        while (_win.pollEvent(evt))
        {
            if ((evt.type == sf::Event::KeyPressed && evt.key.code == sf::Keyboard::Space))//handle space;
            {
                pressed = true;
            } else if ((evt.type == sf::Event::KeyReleased && evt.key.code == sf::Keyboard::Space)) {
                consec_press = 0;
                pressed = false;

            }
        }

        if (pressed) {
            std::cout << "Spaaaaace !" << std::endl;
            sf::Vector2f bpos = _player->getBroomPos();
            int bx = bpos.x / 16.;
            int by = bpos.y / 16.;

            std::cout << bpos.x / 16.f << ' ';
            std::cout << bpos.y / 16.f << std::endl;

            std::cout << bx << ' ';
            std::cout << by << std::endl;

            _map[by][bx]->hit(*_player);
            consec_press += 1;
        }

        //_player->rotate(5.);

        sf::Vector2f pos = _player->getPos();
        sf::Vector2f next = _player->getNextPos();

        int x = pos.x / 16.;
        int y = pos.y / 16.;
        int nx = next.x / 16.;
        int ny = next.y / 16.;

        if (x != nx || y != ny) {
            std::cout << "new pos" << nx << " ";
            std::cout << ny << std::endl;
            switch (_map[ny][nx]->moveOn(*_player)) {
                case GameStatus::reset:
                    load_level();
                    continue;
                case GameStatus::next:
                    ++_level;
                    load_level();
                    continue;
            }
        }

        _player
            ->updatePos()
            .rotate(3.);

        draw();
    }
}

void Game::load_level() {
    _map = _mapLoader.load(_player, std::to_string(_level));
}

void Game::draw() {
    _win.clear();
    for (auto &l : _map) {
        for (auto &e : l) {
            if (e)
                e->draw(_win);
            else continue;
        }
    }

    _player->draw(_win);
    _win.display();
}
