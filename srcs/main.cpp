#include "Game.hpp"

int main(void)
{
    Game game(sf::VideoMode(800, 800));

    game.run();

    return 0;
}
