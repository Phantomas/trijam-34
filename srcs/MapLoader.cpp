#include <iostream>
#include <fstream>
#include <cstdlib>
#include "MapLoader.hpp"
#include "Player.hpp"
#include "Except.hpp"
#include "tiles/Ground.hpp"
#include "tiles/UnbreakableWall.hpp"
#include "tiles/BreakableWall.hpp"
#include "tiles/Exit.hpp"
#include "tiles/Spike.hpp"
#include "tiles/Key.hpp"
#include "tiles/Door.hpp"

MapLoader::MapLoader(TextureManager &tm) noexcept
    : _textureManager(tm), _factory(defaultFactory())
{
}

MapLoader::~MapLoader() {

}

MapLoader::MapLoader(const MapLoader &other)
    : MapLoader(other._textureManager)
{

}

MapLoader &MapLoader::operator=(const MapLoader &) {
    return *this;
}

MapLoader::Map MapLoader::load(player::PlayerPtr &player, const std::string &mapNumber) {
    this->_player = &player;
    std::ifstream ifile(std::string(resourcePath) + base_level_name + mapNumber + extension);
    char buf[16];
    int max_width, max_height;
    Map map;

    if (!ifile.getline(buf, 3))
        throw Except<ExceptType::CantReadFile>(std::string("Can't get width of ") + resourcePath + base_level_name + mapNumber + extension);
    max_width = std::atoi(buf);
    if (!ifile.getline(buf, 3))
        throw Except<ExceptType::CantReadFile>(std::string("Can't get height of ") + resourcePath + base_level_name + mapNumber + extension);
    max_height = std::atoi(buf);

    map.resize(max_height);
    for (auto& elem: map) {
        elem.resize(max_width);
    }
    int x = 0, y = 0;
    while (ifile.get(buf[0])) {
        if (buf[0] == '\n') {
            std::cout << std::endl;
            if (++y == max_height) break;
            x = 0;
            continue;
        }
        std::cout << buf[0];
        //std::cout << buf[0] << std::endl;
        if (_factory.find(buf[0]) != _factory.end()) {
            auto &[filename, function] = this->_factory[buf[0]];
            map[y][x] = function(this->_textureManager, buf[0], filename, std::make_pair(x, y));
        } else {
            map[y][x] = GameEntities::Ground::create(_textureManager, buf[0], "ground", std::make_pair(x, y));
        }
        ++x;
            //std::cout << "not found : '" << buf[0] << "'" << std::endl;
    }

    //this->_player = nullptr;
    ifile.close();
    return map;
}

MapLoader::FactoryType MapLoader::defaultFactory() {
    return {
        { ' ', std::make_tuple<std::string, FunctionType>("ground", &GameEntities::Ground::create) },
        { '0', std::make_tuple<std::string, FunctionType>("wall_0", &GameEntities::UnbreakableWall::create) },
        { '1', std::make_tuple<std::string, FunctionType>("1", &GameEntities::BreakableWall::create) },
        { '2', std::make_tuple<std::string, FunctionType>("2", &GameEntities::BreakableWall::create) },
        { '3', std::make_tuple<std::string, FunctionType>("3", &GameEntities::BreakableWall::create) },
        { '4', std::make_tuple<std::string, FunctionType>("4", &GameEntities::BreakableWall::create) },
        { '5', std::make_tuple<std::string, FunctionType>("5", &GameEntities::BreakableWall::create) },
        { '6', std::make_tuple<std::string, FunctionType>("6", &GameEntities::BreakableWall::create) },
        { '7', std::make_tuple<std::string, FunctionType>("7", &GameEntities::BreakableWall::create) },
        { '8', std::make_tuple<std::string, FunctionType>("8", &GameEntities::BreakableWall::create) },
        { '9', std::make_tuple<std::string, FunctionType>("9", &GameEntities::BreakableWall::create) },
        { 'a', std::make_tuple<std::string, FunctionType>("key", &GameEntities::Key::create) },
        { 'b', std::make_tuple<std::string, FunctionType>("key", &GameEntities::Key::create) },
        { 'c', std::make_tuple<std::string, FunctionType>("key", &GameEntities::Key::create) },
        { 'd', std::make_tuple<std::string, FunctionType>("key", &GameEntities::Key::create) },
        { 'e', std::make_tuple<std::string, FunctionType>("key", &GameEntities::Key::create) },
        { 'f', std::make_tuple<std::string, FunctionType>("door", &GameEntities::Door::create) },
        { 'g', std::make_tuple<std::string, FunctionType>("door", &GameEntities::Door::create) },
        { 'h', std::make_tuple<std::string, FunctionType>("door", &GameEntities::Door::create) },
        { 'i', std::make_tuple<std::string, FunctionType>("door", &GameEntities::Door::create) },
        { 'j', std::make_tuple<std::string, FunctionType>("door", &GameEntities::Door::create) },
        { 'x', std::make_tuple<std::string, FunctionType>("wall_0", &GameEntities::Exit::create) },
        { 'w', std::make_tuple<std::string, FunctionType>("spikes", &GameEntities::Spike::create) },
        { 'p', std::make_tuple<std::string, FunctionType>("player", [this](TextureManager &tm, char key, const std::string &name, std::pair<int, int> pos) -> GameEntities::TilePtr {
            // Create player;
            auto t1 = _textureManager.load(name);
            auto t2 = _textureManager.load("broom");
            *this->_player = std::make_unique<player::Player>(key, pos.first, pos.second, t1, t2);
            return GameEntities::Ground::create(tm, key, "ground", pos);
        }) }
    };
}
