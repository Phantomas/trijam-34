#include "TextureManager.hpp"

TextureManager::TextureManager() {

}

TextureManager::~TextureManager() {
}

TextureManager::TexturePtr TextureManager::load(const std::string &filename) {
    if (_textures.find(filename) == _textures.end()) {
        _textures.emplace(filename, std::make_shared<sf::Texture>());
        _textures[filename]->loadFromFile(std::string(resourcePath) + filename + extension);
    }
    return _textures[filename];
}
