#include "Dispatcher.hpp"

namespace events {
    Dispatcher & Dispatcher::getDispatcher() {
        static Dispatcher dis;

        return dis;
    }


    Dispatcher::Dispatcher(): _map(), _idx(0) {}

    uint32_t Dispatcher::register_callback(std::type_index const &idx, Callback &&fn) {
        uint32_t id = _idx++;

        _map[idx].emplace_back(id, fn);

        return id;
    }

    void Dispatcher::unregister_callback(std::type_index const &idx, uint32_t const cid) {
        if (_map.find(idx) != _map.end()) {
            auto & vec = _map.at(idx);
            vec.erase(std::remove_if(vec.begin(), vec.end(), [&cid](auto const v) { return v.first == cid; }), vec.end());
        }

        return ;
    }

    void Dispatcher::dispatch(std::type_index const & idx, std::any const &param) const {
        if (_map.find(idx) != _map.end())
            for (auto const & v : _map.at(idx))
                v.second(param);
    }

    void Dispatcher::loop() {
        for (auto const &kv : _queues) {
            for (auto const &evt : kv.second)
                dispatch(kv.first, evt);
        }
    }

    Dispatcher & Dispatcher::queue_event(std::type_index const &idx, std::any const &param) {
        _queues[idx].push_back(param);

        return *this;
    }
}
