#ifndef DISPATCHER_HPP_
#define DISPATCHER_HPP_

#include <any>
#include <cstdint>
#include <functional>
#include <typeindex>

namespace events {
    class Dispatcher {
        public:
            using Callback = std::function<void(std::any const &)>;
            Dispatcher();

            uint32_t register_callback(std::type_index const &, Callback &&);
            void unregister_callback(std::type_index const &, uint32_t const);

            void loop();

            Dispatcher & queue_event(std::type_index const &, std::any const &);

            template<class T>
            uint32_t register_callback(Callback &&);
            template<class T>
            void unregister_callback(uint32_t const);

            template<class T>
            void dispatch(T const &&) const;
            template <class T>
            void dispatch(std::any const &p) const;
            void dispatch(std::type_index const &, std::any const &) const;

            static Dispatcher & getDispatcher();
        private:

            std::unordered_map<std::type_index, std::vector<std::pair<uint32_t, Callback>>> _map;
            std::unordered_map<std::type_index, std::vector<std::any>> _queues;
            uint32_t _idx;
    };

    template<class T>
        uint32_t Dispatcher::register_callback(Callback &&c) {
            return register_callback(typeid(T), std::forward<Callback &&>(c));
        }

    template<class T>
        void Dispatcher::unregister_callback(uint32_t const c) {
            unregister_callback(typeid(T), c);
        }

    template<class T>
        void Dispatcher::dispatch(T const &&p) const {
            dispatch(typeid(T), std::any(p));
        }

    template<class T>
        void Dispatcher::dispatch(std::any const &p) const {
            dispatch(typeid(T), p);
        }
}

#endif
