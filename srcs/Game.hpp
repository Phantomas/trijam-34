#ifndef GAME_HPP_
#define GAME_HPP_

#include <memory>

#include <SFML/Graphics.hpp>
#include <SFML/Audio/Music.hpp>

#include "MapLoader.hpp"
#include "Player.hpp"
#include "GameStatus.hpp"


class Game {
    public:
        Game(sf::VideoMode mode);
        ~Game() {}

        void run();
    private:
        void draw();
        void load_level();

        sf::RenderWindow _win;
        player::PlayerPtr _player;
        MapLoader::Map  _map;
        int _level;
        sf::Music   _musicPlayer;
        TextureManager _textMngr;
        MapLoader      _mapLoader;
};

#endif
