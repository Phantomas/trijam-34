#pragma once

#include <vector>
#include <unordered_map>
#include <tuple>
#include <functional>
#include "Player.hpp"
#include "tiles/Tile.hpp"

class TextureManager;

class MapLoader {
public:
    using Map = std::vector<std::vector<GameEntities::TilePtr>>;
    static constexpr char resourcePath[16] = "resources/maps/";
    static constexpr char base_level_name[7] = "level_";
    static constexpr char extension[5] = ".map";

protected:
    using FunctionType = std::function<GameEntities::TilePtr(TextureManager &, char, const std::string &, std::pair<int, int>)>;
    using FactoryType = std::unordered_map<char, std::tuple<std::string, FunctionType>>;

public:
    MapLoader() = delete;
    MapLoader(TextureManager &) noexcept;
    ~MapLoader();
    MapLoader(const MapLoader &);

public:
    MapLoader &operator=(const MapLoader &);

public:
    /*
     * Takes a reference to the player and the map number as a std::string
     * then load the map and returns it, it also calls the operator= on the
     * player reference.
     */
    Map load(player::PlayerPtr &, const std::string &);

protected:
    FactoryType defaultFactory();

protected:
    TextureManager  &_textureManager;
    FactoryType     _factory;
    player::PlayerPtr *_player = nullptr;
};
