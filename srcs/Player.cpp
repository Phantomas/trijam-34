#include <cmath>

#include "Player.hpp"

namespace player {
    Player::Player(char key, int posx, int posy, TextureManager::TexturePtr &self, TextureManager::TexturePtr &broom):
        Player(key, {(float)posx, (float)posy}, self, broom)
    {};

    Player::Player(char, sf::Vector2f pos, TextureManager::TexturePtr &self, TextureManager::TexturePtr &broom):
        _angle(0.f), _initial_pos(pos * 16.f), _pos(pos * 16.f), _speed(0.f, 0.f),
        _self(*self), _self_text(self),
        _broom(*broom), _broom_text(self) {
            _initial_pos.x += 8.f;
            _initial_pos.y += 8.f;

            _pos.x += 8.f;
            _pos.y += 8.f;

            _self.setOrigin(8., 8.);
            _self.setPosition(_pos );
            _broom.setOrigin(-8., 8.);
            _broom.setPosition(_pos);
        }

    void Player::reset() {
        _pos = _initial_pos;
        _speed = {0., 0.};
    }

    void Player::draw(sf::RenderWindow &win) {
        win.draw(_broom);
        win.draw(_self);
    }

    Player & Player::move() {
        float f = 0.2;
        _speed = {-1.f * 16.f * f * std::cos(_angle * M_PI / 180.f), -1. * 16.f * f * std::sin(_angle * M_PI / 180.f)};
        return *this;
    }

    Player& Player::stop() {
        _speed = {0., 0.};
        return *this;
    };

    Player & Player::rotate(float angle) {
        _angle = std::fmod(angle + _angle, 360.f);
        _self.rotate(angle);
        _broom.rotate(angle);

        return *this;
    }

    sf::Vector2f Player::getBroomPos() {
        return {_pos.x + 8.1f * std::cos(_angle * M_PI / 180.f), _pos.y + 8.1f * std::sin(_angle * M_PI / 180.f) };
    }

    sf::Vector2f Player::getNextPos() {
        return _pos + _speed;
    }

    Player & Player::updatePos() {
        _pos = getNextPos();
        _self.setPosition(_pos);
        _broom.setPosition(_pos);

        return *this;
    }

    sf::Vector2f Player::getPos() {
        return _pos;
    }

    sf::Vector2f Player::getSpeed() {
        return _speed;
    }

    float Player::getAngle() {
        return _angle;
    }
}

