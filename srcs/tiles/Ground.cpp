#include "Ground.hpp"

namespace GameEntities {
    TilePtr Ground::create(TextureManager &t_manager, char, std::string const &name, Pos pos) noexcept {
        return std::make_shared<Ground>(std::move(pos), t_manager.load(name));
    }

    void Ground::hit(Player &) noexcept {
        return ;
    }

    GameStatus Ground::moveOn(Player &) noexcept {
        return GameStatus::cont;
    }
}
