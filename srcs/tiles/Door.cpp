#include "Door.hpp"

namespace GameEntities {
    TilePtr Door::create(TextureManager &t_manager, char k, const std::string &name, Pos pos) noexcept {
        return std::make_shared<Door>(pos, k, t_manager.load(name), t_manager.load("ground"));
    }

    void Door::hit(Player &player) noexcept {
        if (!this->_open)
            player.move();
    }

    GameStatus Door::moveOn(Player &player) noexcept {
        if (!this->_open)
            player.stop();
        return GameStatus::cont;
    }

    void Door::open(char k) noexcept {
        if (k == this->k) {
            this->_open = true;
            this->sprite.setTexture(*this->opened);
        }
    }
}
