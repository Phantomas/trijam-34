#include<string>
#include "Tile.hpp"

#ifndef SPIKE_HPP__
# define SPIKE_HPP__

namespace GameEntities {
    class Spike: public Tile {
        TexturePtr texPtr;

    public:
        Spike(Pos pos, TexturePtr texPtr): Tile(std::move(pos), texPtr), texPtr(texPtr) {}
        ~Spike() {}

        static TilePtr create(TextureManager&, char, const std::string&, Pos) noexcept;

        void hit(Player&) noexcept;
        GameStatus moveOn(Player&) noexcept;
    };
}

#endif
