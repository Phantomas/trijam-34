#include "Tile.hpp"
#include <string>

#ifndef UNBREAKABLEWALL_HPP__
# define UNBREAKABLEWALL_HPP__

namespace GameEntities {
    class UnbreakableWall: public Tile {
        TexturePtr texPtr;

    public:
        UnbreakableWall(Pos pos, TexturePtr texPtr): Tile(std::move(pos), texPtr), texPtr(texPtr) {}
        ~UnbreakableWall() {}

        static TilePtr create(TextureManager&, char, const std::string&, Pos) noexcept;

        void hit(Player&) noexcept;
        GameStatus moveOn(Player&) noexcept;
    };
}

#endif
