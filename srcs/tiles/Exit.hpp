#include <string>
#include "Tile.hpp"

#ifndef EXIT_HPP__
#define EXIT_HPP__

namespace GameEntities {
    class Exit: public Tile {
        TexturePtr texPtr;

    public:
        Exit(Pos pos, TexturePtr texPtr): Tile(std::move(pos), texPtr), texPtr(texPtr) {}
        virtual ~Exit() {}

        static TilePtr create(TextureManager&, char, const std::string&, Pos) noexcept;

        void hit(Player&) noexcept override {}
        GameStatus moveOn(Player&) noexcept override;
    };
}

#endif
