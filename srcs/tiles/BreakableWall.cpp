#include <array>
#include "BreakableWall.hpp"

namespace GameEntities {
    BreakableWall::BreakableWall(Pos pos, std::array<TexturePtr, 10> &texPtrs, int hp): Tile(std::move(pos), texPtrs[hp]), hp(hp) {}

    TilePtr BreakableWall::create(TextureManager &t_manager, char k, const std::string &name, Pos pos) noexcept {
        std::array<TexturePtr, 10> texPtrs;

        texPtrs['0'] = t_manager.load("ground");
        for (char i = '1'; i <= k; i++ ) {
            texPtrs[i - '0'] = t_manager.load(std::string(1, i));
        }

        return std::make_shared<BreakableWall>(pos, texPtrs, k - '0');
    }

    void BreakableWall::hit(Player &player) noexcept {
        if (this->hp > 0) {
            player.move();
            this->hp -= 1;
            this->sprite.setTexture(*this->texPtrs[this->hp].get());
        }
    }

    GameStatus BreakableWall::moveOn(Player &player) noexcept {
        if (this->hp > 0)
            player.stop();
        return GameStatus::cont;
    }
}
