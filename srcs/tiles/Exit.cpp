#include "Exit.hpp"

namespace GameEntities {
    TilePtr Exit::create(TextureManager &t_manager, char k, const std::string &name, Pos pos) noexcept {
        return std::make_shared<Exit>(pos, t_manager.load(name));
    }

    GameStatus Exit::moveOn(Player &) noexcept {
        return GameStatus::next;
    }

}
