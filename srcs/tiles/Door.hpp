#include <string>
#include "Tile.hpp"

#ifndef DOOR_HPP__
#define DOOR_HPP__

namespace GameEntities {
    class Door: public Tile {
        TexturePtr opened;
        TexturePtr closed;
        char k;
        bool _open{false};

    public:
        Door(Pos pos, char, TexturePtr closed, TexturePtr opened): Tile(std::move(pos), closed), k(k), opened(opened), closed(closed) {}
        ~Door() {}

        static TilePtr create(TextureManager&, char, const std::string&, Pos) noexcept;

        void hit(Player&) noexcept;
        GameStatus moveOn(Player&) noexcept;
        void open(char) noexcept;
    };
}

#endif
