#include <string>
#include <array>
#include "Tile.hpp"

#ifndef BREAKABLEWALL_HPP__
# define BREAKABLE_WALL_HPP__

namespace GameEntities {
    class BreakableWall: public Tile {
        TexturePtr texPtr;
        std::array<TexturePtr, 10> texPtrs;

        int hp;

    public:
        BreakableWall(Pos, std::array<TexturePtr, 10>&, int);
        virtual ~BreakableWall() {}

        static TilePtr create(TextureManager &, char, const std::string&, Pos) noexcept;

        //void draw(sf::RenderWindow&) const noexcept;
        void hit(Player&) noexcept override;
        GameStatus moveOn(Player&) noexcept override;
    };
}

#endif
