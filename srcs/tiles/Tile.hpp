#ifndef TILE_HPP__
# define TILE_HPP__

#include <memory>
#include <utility>
#include <SFML/Graphics.hpp>
#include "../Player.hpp"
#include "../TextureManager.hpp"
#include "../GameStatus.hpp"

namespace GameEntities {
    using TexturePtr = TextureManager::TexturePtr;
    using Pos = std::pair<int, int>;
    using Player = player::Player;

    class Tile {
    protected:
        const int tile_sz = 16;
        std::pair<int, int> pos;
        sf::Sprite sprite;

    public:
        Tile(Pos, TexturePtr);
        virtual ~Tile() {}

        virtual void draw(sf::RenderWindow&) const noexcept;
        virtual void hit(Player&) noexcept = 0;
        virtual GameStatus moveOn(Player&) noexcept = 0;
    };

    using TilePtr = std::shared_ptr<Tile>;
}

#endif
