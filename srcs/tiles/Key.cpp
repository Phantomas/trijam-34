#include "Key.hpp"

namespace GameEntities {
    TilePtr Key::create(TextureManager &t_manager, char k, const std::string &name, Pos pos) noexcept {
        return std::make_shared<Key>(pos, k, t_manager.load(name), t_manager.load("ground"));
    }

    void Key::hit(Player &player) noexcept {
        if (!this->fetched) {
            this->fetched = true;
            this->sprite.setTexture(*this->ground.get());
        }
    }

    GameStatus Key::moveOn(Player &player) noexcept {
        this->hit(player);
        return GameStatus::cont;
    }
}
