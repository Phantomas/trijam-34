#include <string>
#include <utility>
#include "Tile.hpp"

#ifndef GROUND_HPP__
# define GROUND_HPP__

namespace GameEntities {
    class Ground : public Tile {
        TexturePtr texPtr;

        public:
            Ground(Pos pos, TexturePtr texPtr): Tile(std::move(pos), texPtr), texPtr(texPtr) {}
            ~Ground() {}

            static TilePtr create(TextureManager&, char, const std::string&, Pos) noexcept;

            void hit(Player&) noexcept;
            GameStatus moveOn(Player&) noexcept;
    };
}

#endif
