#include "Tile.hpp"

namespace GameEntities {
    Tile::Tile(Pos pos, TexturePtr texture): pos(pos), sprite(sf::Sprite()) {
        this->sprite.setTexture(*texture.get());
        this->sprite.setPosition(pos.first * this->tile_sz, pos.second * this->tile_sz);
    }

    void Tile::draw(sf::RenderWindow &window) const noexcept {
        window.draw(this->sprite);
    }
}
