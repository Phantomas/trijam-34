#include "Spike.hpp"

namespace GameEntities {
    TilePtr Spike::create(TextureManager &t_manager, char k, const std::string &name, Pos pos) noexcept {
        return std::make_shared<Spike>(pos, t_manager.load(name));
    }

    void Spike::hit(Player &player) noexcept {
        player.move();
    }

    GameStatus Spike::moveOn(Player &player) noexcept {
        return GameStatus::reset;
    }
}
