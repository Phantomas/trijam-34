#include "UnbreakableWall.hpp"

namespace GameEntities {
    TilePtr UnbreakableWall::create(TextureManager &t_manager, char, const std::string &name, Pos pos) noexcept {
        return std::make_shared<UnbreakableWall>(std::move(pos), t_manager.load(name));
    }

    void UnbreakableWall::hit(Player &player) noexcept {
        player.move();
    }

    GameStatus UnbreakableWall::moveOn(Player &player) noexcept {
        player.stop();
        return GameStatus::cont;
    }
}
