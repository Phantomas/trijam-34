#include <string>
#include "Tile.hpp"

#ifndef KEY_HPP__
#define KEY_HPP__

namespace GameEntities {
    class Key: public Tile {
        TexturePtr key;
        TexturePtr ground;
        char k;
        bool fetched{false};

    public:
        Key(Pos pos, char k, TexturePtr key, TexturePtr ground): Tile(std::move(pos), key), k(k), key(key), ground(ground) {}
        ~Key() {}

        static TilePtr create(TextureManager&, char, const std::string&, Pos) noexcept;

        void hit(Player&) noexcept;
        GameStatus moveOn(Player&) noexcept;
    };
}

#endif
