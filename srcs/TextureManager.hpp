#pragma once

#include <memory>
#include <unordered_map>
#include <SFML/Graphics/Texture.hpp>

class TextureManager {
public:
    using TexturePtr = std::shared_ptr<sf::Texture>;

protected:
    static constexpr char resourcePath[20] = "resources/textures/";
    static constexpr char extension[5] = ".png";

public:
    TextureManager();
    TextureManager(const TextureManager &) = delete;
    ~TextureManager();

public:
    TextureManager &operator=(const TextureManager &) = delete;

public:
    TexturePtr load(const std::string &);

protected:
    std::unordered_map<std::string, TexturePtr>     _textures;
};
