#pragma once

#include <exception>

enum class ExceptType {
    CantReadFile,
    BadMapFormat
};

template <ExceptType type>
class Except: std::exception {
public:
    Except(std::string &&precisions) : _precisions(std::move(precisions)) {}
    ~Except() override {}
    Except &operator=(const Except<type> &other) noexcept
    { this->_precisions = other.precisions; }
    const char *what() const noexcept override { return _precisions.c_str(); }
protected:
    std::string     _precisions;
};
