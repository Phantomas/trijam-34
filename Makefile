NAME = broom

CC	=	g++

BUILDDIR	=	build

SRCS	=	srcs/main.cpp
SRCS	+=	srcs/Game.cpp
SRCS	+=	srcs/MapLoader.cpp
SRCS	+=	srcs/Player.cpp
SRCS	+=	srcs/TextureManager.cpp
SRCS	+=	srcs/tiles/BreakableWall.cpp
SRCS	+=	srcs/tiles/Ground.cpp
SRCS	+=	srcs/tiles/Tile.cpp
SRCS	+=	srcs/tiles/Exit.cpp
SRCS	+=	srcs/tiles/UnbreakableWall.cpp
SRCS	+=	srcs/tiles/Spike.cpp
SRCS	+=	srcs/tiles/Door.cpp
SRCS	+=	srcs/tiles/Key.cpp


OBJS	=	$(patsubst %.cpp, $(BUILDDIR)/%.o, $(SRCS))

CXXFLAGS	=	-std=c++17 -Wall -Wextra -O2

LDFLAGS	=	-lsfml-system -lsfml-window -lsfml-graphics -lsfml-audio

NO_COLOR	=	"\033[0m"
GREEN_COLOR	=	"\033[0;32m"
RED_COLOR	=	"\033[0;31m"
YELLOW_COLOR	=	"\033[0;33m"

ECHO	=	echo

all: $(NAME)

$(BUILDDIR)/%.o:		%.cpp
	@mkdir -p $(shell dirname $@)
	@$(ECHO) $(GREEN_COLOR)[C]$(NO_COLOR) $@ "<-" $(YELLOW_COLOR)$<$(NO_COLOR)
	@$(CC) -c -o $@ $< $(CXXFLAGS)

$(NAME):	$(OBJS)
	@$(ECHO) $(GREEN_COLOR)[L]$(NO_COLOR) $@ "<-" $(YELLOW_COLOR)$<$(NO_COLOR)
	@$(CC) -o $(NAME) $(OBJS) $(LDFLAGS)

clean:
	@$(ECHO) $(RED_COLOR)[X]$(NO_COLOR) $(YELLOW_COLOR)$(OBJS)$(NO_COLOR)
	@$(RM) $(OBJS)
	@$(ECHO) $(RED_COLOR)[X]$(NO_COLOR) $(YELLOW_COLOR)$(BUILDDIR)$(NO_COLOR)
	@$(RM) -r $(BUILDDIR)

fclean:	clean
	@$(ECHO) $(RED_COLOR)[X]$(NO_COLOR) $(YELLOW_COLOR)$(NAME)$(NO_COLOR)
	@$(RM) $(NAME)

re:	fclean all

.PHONY:	all clean fclean re
